PHONY_TARGETS=help update-version fetch-tarball check fetch gitlab-ci
.PHONY : $(PHONY_TARGETS)

help:
	@echo "Use: make $(PHONY_TARGETS)"

version:=$(shell cat version)
release:=$(shell cat release)
bsys6_release:=$(shell cat bsys6_release)
full_version:=$(version)-$(release)$(shell [ $(bsys6_release) -gt 1 ] && echo "-$(bsys6_release)")
ifeq ($(arch),)
arch:=x86_64
endif

artifact=librewolf-$(full_version).source.tar.gz
check : update-version
update-version :
	@wget -qO version "https://gitlab.com/librewolf-community/browser/source/-/raw/main/version"
	@wget -qO release "https://gitlab.com/librewolf-community/browser/source/-/raw/main/release"
	@echo "Now using LibreWolf version $$(cat version)-$$(cat release)."

fetch : fetch-tarball
fetch-tarball :
	@wget -qO "librewolf-$$(cat version)-$$(cat release).source.tar.gz.sha256sum" "https://gitlab.com/librewolf-community/browser/source/-/jobs/artifacts/main/raw/librewolf-$$(cat version)-$$(cat release).source.tar.gz.sha256sum?job=Build"
	@wget -qO "librewolf-$$(cat version)-$$(cat release).source.tar.gz.sig" "https://gitlab.com/librewolf-community/browser/source/-/jobs/artifacts/main/raw/librewolf-$$(cat version)-$$(cat release).source.tar.gz.sig?job=Build"
	@wget --progress=bar:force -O "librewolf-$$(cat version)-$$(cat release).source.tar.gz" "https://gitlab.com/librewolf-community/browser/source/-/jobs/artifacts/main/raw/librewolf-$$(cat version)-$$(cat release).source.tar.gz?job=Build"
	cat "librewolf-$$(cat version)-$$(cat release).source.tar.gz.sha256sum"
	sha256sum -c "librewolf-$$(cat version)-$$(cat release).source.tar.gz.sha256sum"
	gpg --import librewolf.asc
	gpg --verify "librewolf-$$(cat version)-$$(cat release).source.tar.gz.sig" "librewolf-$$(cat version)-$$(cat release).source.tar.gz"

gitlab-ci :
	@echo "Makefile: Error: Target not implemented yet."


#PHONY_TARGETS=all mockup veryclean linux macos windows help clean update-version fetch-tarball collect not-docker not-docker-linux not-docker-macos not-docker-windows check fetch distro
#.PHONY : $(PHONY_TARGETS)



#all : mockup
all : fetch-tarball linux macos windows


linux :
	cp -v $(artifact) linux
	$(MAKE) -C linux arch=$(arch) all
macos :
	cp -v $(artifact) macos
	$(MAKE) -C macos arch=$(arch) all
windows :
	cp -v $(artifact) windows
	$(MAKE) -C windows arch=$(arch) all


veryclean :
	rm -f "librewolf-$$(cat version)-$$(cat release).source.tar.gz" "librewolf-$$(cat version)-$$(cat release).source.tar.gz.sha256sum" "librewolf-$$(cat version)-$$(cat release).source.tar.gz.sig"
	$(MAKE) -C linux veryclean
	$(MAKE) -C macos veryclean
	$(MAKE) -C windows veryclean

clean :
	$(MAKE) -C linux clean
	$(MAKE) -C macos clean
	$(MAKE) -C windows clean


#
# Non-recursive targets
#





#
# `make mockup` creates text files containing the word `mockup` for the artifact file(s).
#

mockup :
	echo -n "" > $(artifact)
	$(MAKE) -C linux arch=$(arch) mockup
	$(MAKE) -C macos arch=$(arch) mockup
	$(MAKE) -C windows arch=$(arch) mockup


collect :
	@echo todo: grab all generated artifacts into the artifacts/ folder.

not-docker : $(artifact) not-docker-linux not-docker-macos not-docker-windows
not-docker-linux:
	cp -v $(artifact) linux
	$(MAKE) -C linux arch=$(arch) not-docker
not-docker-macos:
	cp -v $(artifact) macos
	$(MAKE) -C macos arch=$(arch) not-docker
not-docker-windows:
	cp -v $(artifact) windows
	$(MAKE) -C windows arch=$(arch) not-docker

distro :
	./pkg/distro default
