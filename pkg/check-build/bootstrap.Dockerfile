FROM base:latest

# grab the latest source cleanly
WORKDIR /
RUN rm -rf /source && git clone --recursive https://gitlab.com/librewolf-community/browser/source.git

# do the bootstrap and clean up (trying) to minimise image size
WORKDIR /source
RUN make fetch && make bootstrap veryclean

COPY build.sh /
