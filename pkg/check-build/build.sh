#!/bin/sh

set -e

cd /
rm -rf /source && git clone --recursive https://gitlab.com/librewolf-community/browser/source.git

cd /source
make fetch && make build package
