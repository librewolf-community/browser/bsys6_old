#!/usr/bin/python3

import sys
import os
from pathlib import Path


def which(cmd):
    exec_path = os.get_exec_path()
    for dir in exec_path:
        try:
            for child in Path(dir).iterdir():
                if child.is_file():
                    name = child.name
                    if name == cmd:
                        return f'{dir}/{name}'
        except:
            pass
    return None

def shell(cmd, do_print = True, keep_going = False):
    if do_print:
        print(cmd)
        
    sys.stdout.flush()
    retval = os.system(cmd)
    sys.stdout.flush()
    
    if retval != 0 and not keep_going:
        sys.exit(retval)



pkg_available = [
    {'name':'apt-get', 'update':'<apt>', 'install':'-y install'},
    {'name':'dnf', 'update':'-y update', 'install':'-y install'},
    {'name':'pacman', 'update':'--noconfirm -Syu', 'install':'--noconfirm -Syu'},
    {'name':'zypper', 'update':'-n up', 'install':'-n in'},
]

pkg_map = [
    {'name':'vim',  'apt-get':'vim','dnf':'vim','pacman':'vim','zypper':'vim'},
    {'name':'make', 'apt-get':'make','dnf':'make','pacman':'make','zypper':'make'},
    {'name':'git',  'apt-get':'git','dnf':'git','pacman':'git','zypper':'git'},
    {'name':'xz',   'apt-get':'xz-utils','dnf':'xz','pacman':'xz','zypper':'xz'},
    {'name':'curl', 'apt-get':'curl','dnf':'curl','pacman':'curl','zypper':'curl'},
    {'name':'gcc',  'apt-get':'gcc','dnf':'gcc','pacman':'gcc','zypper':'gcc'},
    {'name':'gnupg','apt-get':'gnupg','dnf':'gnupg','pacman':'gnupg','zypper':'gnupg'},
]

pkg_sets = [
    {'name':'',     'list':['','','',]},
]

pm_name = ''
pm_exe = ''
pm_update_cmds = ''
pm_install_cmd = ''



def install_packages(packages):
    pkg_list = ''
    for package in packages:
        found = False
        for p in pkg_map:
            if p['name'] == package:
                pkg_list = pkg_list + ' ' + p[pm_name]
                found = True
        if not found:
            print(f'warning: package \'{package}\' not found.')
            pkg_list = pkg_list + ' ' + package
    shell(format(f'{pm_install_cmd} {pkg_list}'))

def setup_rust():
    install_packages(['curl', 'gcc'])
    tmpfile = '/tmp/tmp.784979489.sh' # can do better in python
    shell(f'curl --proto \'=https\' --tlsv1.2 -sSf https://sh.rustup.rs > {tmpfile}')
    shell(f'chmod +x {tmpfile}')
    shell(f'/bin/sh -c "{tmpfile} -y"')
    shell(f'rm -f {tmpfile}')


def detect_pkg():
    for p in pkg_available:
        q = which(p['name'])
        if q != None:
            global pm_name, pm_exe, pm_update_cmds, pm_install_cmd
            pm_name = p['name']
            pm_exe = q
            tmp = p['install']
            pm_install_cmd = f'{pm_exe} {tmp}'
            if p['update'] == '<apt>':
                pm_update_cmds = f'{pm_exe} update && {pm_exe} -y upgrade'
            else:
                tmp = p['update']
                pm_update_cmds = f'{pm_exe} {tmp}'
            return q
    return None

if detect_pkg() == None:
    print('fatal error: this OS is using an unknown package manager.\n')
else:

    for arg in sys.argv[1:]:
        if arg == 'update':
            shell(f'{pm_update_cmds}')
        else:
            if arg == 'rust':
                setup_rust()
            else:
                install_packages([arg])


