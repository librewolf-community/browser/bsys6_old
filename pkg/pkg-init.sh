#!/bin/sh

# you can't polish a turd..

update_system()
{
    ( pacman --noconfirm -Syu ; true )
    ( apt-get update && apt-get -y upgrade ; true )
    ( dnf -y update && dnf -y install make wget screen ; true )
    ( zypper -n up && zypper -n in vim make wget screen ; true )
}

install_packages()
{
    ( pacman --noconfirm -Syu $* ; true )
    ( apt-get -y install $* ; true )
    ( dnf -y install $* ; true )
    ( zypper -n in $* ; true )
}

setup_rust()
{
    install_packages curl gcc
    tmpfile=/tmp/tmp.784979489.sh
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs > $tmpfile
    chmod +x $tmpfile
    /bin/sh -c "$tmpfile -y"
    rm -f $tmpfile
}

setup_mach()
{
  # dependencies needed to run ./mach bootstrap
  ( apt-get -y install python3 python3-dev python3-pip wget dpkg-sig ; true )
  ( dnf -y install python3 python3-devel wget rpm-build rpm-sign ; true )
  ( zypper -n in mercurial python3 python3-pip python3-devel wget rpm-build ; true )
}

usage()
{
    echo "use: setup-env [boot] [update] [essentials] [mach] [netutils] [devutils] [emacs] [rust] [extra]"
}

#
# main functionality
#

if [ $# -eq 0 ]; then
    update_system
    install_packages git vim make curl wget python3
    exit 0
fi

for set in $*
do
    case $set in
	help) usage ; exit 1 ;;
	boot) update_system ; install_packages python3 ;;
	devutils)
	    install_packages git vim make curl wget screen	    
	    install_packages automake autoconf libtool gettext gcc g++ gdb clang cmake
	    ;;
	rust) setup_rust ;;
	emacs) install_packages emacs ;;
	essentials) install_packages vim make curl wget screen git gnupg;;
	mach) setup_mach ;;
	netutils) install_packages whois traceroute ;;
	update) update_system ;;
	extra) install_packages mc links lftp lzip zip unzip ;;
	*) install_packages $set ;;
    esac
done
